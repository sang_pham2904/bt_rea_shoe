import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  // render item shoe ra màn hình khi load trang

  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          shoe={item}
          handleChangeDetail={this.props.handleChangeDetail}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };

  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
