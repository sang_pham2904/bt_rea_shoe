import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { shoe } = this.props;
    return (
      <div className="col-3 my-2">
        <div>
          <div className="card text-center">
            <div className="d-flex justify-content-center">
              <img
                className="card-img-top"
                style={{ width: "10vw" }}
                src={shoe.image}
              />
            </div>
            <div className="card-body">
              <h4 style={{ height: "70px" }} className="card-title">
                {shoe.name}
              </h4>
              <h6 className="card-text pb-4">
                $ {shoe.price.toLocaleString()}
              </h6>
              <p>{shoe.shortDescription}</p>
              <button
                onClick={() => this.props.handleAddToCart(shoe)}
                className="btn btn-primary mx-2"
              >
                Add to cart
              </button>
              <button
                onClick={() => this.props.handleChangeDetail(shoe)}
                className="btn btn-warning"
                data-toggle="modal"
                data-target="#exampleModal"
              >
                View Detail
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
