import React, { Component } from "react";
import CartShoe from "./CartShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";
import { shoeArr } from "./data";
export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr,
    detail: shoeArr[0],
    cart: [],
  };
  //   Khi click nút View Detail
  handleChangeDetail = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleChangeQuantity = (id, choice) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    if (index != -1) {
      cloneCart[index].soLuong = cloneCart[index].soLuong + choice;
    }
    this.setState({
      cart: cloneCart,
    });
    cloneCart[index].soLuong === 0 && this.handleDeleteCart(id);
  };
  handleDeleteCart = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    if (index != -1) {
      cloneCart.splice(index, 1);
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleBuy = () => {
    this.setState({
      cart: [],
    });
    alert("Thank you for your purchase");
  };
  render() {
    let isShowCart = this.state.cart.length;
    return (
      <div>
        <div className="row">
          <div className="col-11">
            <ListShoe
              shoeArr={this.state.shoeArr}
              handleChangeDetail={this.handleChangeDetail}
              handleAddToCart={this.handleAddToCart}
            />
          </div>
          <div className="col-1">
            <CartShoe
              isShowCart={isShowCart}
              cart={this.state.cart}
              handleChangeQuantity={this.handleChangeQuantity}
              handleDeleteCart={this.handleDeleteCart}
              handleBuy={this.handleBuy}
            />
          </div>
        </div>
        <div>
          <DetailShoe
            detail={this.state.detail}
            handleAddToCart={this.handleAddToCart}
          />
        </div>
      </div>
    );
  }
}
