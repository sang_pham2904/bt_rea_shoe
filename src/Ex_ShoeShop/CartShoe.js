import React, { Component } from "react";

export default class CartShoe extends Component {
  renderCart = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: "5vw" }} src={item.image} alt="..." />
          </td>
          <td>$ {(item.price * item.soLuong).toLocaleString()}</td>
          <td>
            <button
              onClick={() => this.props.handleChangeQuantity(item.id, -1)}
              className="btn border-warning"
            >
              -
            </button>
            <span> {item.soLuong} </span>
            <button
              onClick={() => this.props.handleChangeQuantity(item.id, 1)}
              className="btn border-primary"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => this.props.handleDeleteCart(item.id)}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  renderTieuDeCart = () => {
    if (this.props.isShowCart) {
      return (
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Price</th>
            <th>Số Lượng</th>
            <th>Action</th>
          </tr>
        </thead>
      );
    }
  };
  prepareBill = () => {
    //   let total = 0
    //  this.props.cart[i].soLuong * this.props.cart[i].price

    let tongGia = this.props.cart.reduce((tg, item) => {
      return (tg += item.price * item.soLuong);
    }, 0);
    return tongGia.toLocaleString();
  };
  tinhTongSoLuong = () => {
    let tongSoLuong = this.props.cart.reduce((tsl, item) => {
      return (tsl += item.soLuong);
    }, 0);
    return tongSoLuong;
  };
  render() {
    return (
      <div>
        <button
          style={{ position: "fixed", top: "20px", right: "1%" }}
          type="button"
          className="btn btn-info"
          data-toggle="modal"
          data-target="#cartShoe"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            fill="currentColor"
            className="bi bi-cart-check-fill"
            viewBox="0 0 16 16"
          >
            <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-1.646-7.646-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708.708z" />
          </svg>{" "}
          Cart {"(" + this.tinhTongSoLuong() + ")"}
        </button>
        <div
          className="modal fade"
          id="cartShoe"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-xl">
            <div className="modal-content">
              <div className="modal-header text-center">
                <h5 className="modal-title " id="exampleModalLabel">
                  Your cart
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table">
                  {this.renderTieuDeCart()}

                  <tbody>{this.renderCart()}</tbody>
                </table>
              </div>
              <h4 className="text-right pr-3">Total: ${this.prepareBill()}</h4>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  onClick={this.props.handleBuy}
                  className="btn btn-primary"
                >
                  Buy
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
