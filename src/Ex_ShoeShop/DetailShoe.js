import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, shortDescription, quantity, image } =
      this.props.detail;
    return (
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Detail Shoe
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="card text">
                <div className="d-flex justify-content-center">
                  <img
                    className="card-img-top"
                    style={{ width: "300px" }}
                    src={image}
                    alt="Card image cap"
                  />
                </div>
                <div className="card-body">
                  <h4 className="card-title">{name}</h4>
                  <h6 className="card-text">$ {price.toLocaleString()}</h6>
                </div>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item">
                    Current inventory: {quantity}
                  </li>
                  <li className="list-group-item">{description}</li>
                  <li className="list-group-item">{shortDescription}</li>
                </ul>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                onClick={() => this.props.handleAddToCart(this.props.detail)}
                className="btn btn-primary"
              >
                Add to cart
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
